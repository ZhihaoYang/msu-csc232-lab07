//  Created by Frank M. Carrano and Timothy M. Henry.
//  Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.
// Modified by: Zhihao Yang, Jiapeng Lu


/** Implementation file for the class ArrayStack.
@file ArrayStack.cpp */

#include "ArrayStack.h"  // Stack class specification file

template<class ItemType>
ArrayStack<ItemType>::ArrayStack() : top(-1)
{
}  // end default constructor

template<class ItemType>
ArrayStack<ItemType>::ArrayStack(int k) : top(-1), factor(k)
{
} // end parameterized constructor

// Copy constructor and destructor are supplied by the compiler

template<class ItemType>
bool ArrayStack<ItemType>::isEmpty() const
{
    return top < 0;
}  // end isEmpty

template<class ItemType>
bool ArrayStack<ItemType>::push(const ItemType& newEntry)
{
	if (top >= MAX_STACK - 1)
	{
		ItemType* oldArray = items;
		
		if (factor == -1)
		{
			items = new ItemType[2 * (top - 1)];
		}
		else
		{
			items = new ItemType[factor + (top - 1)];
		}
		
		for (int i = 0; i < top; i++)
		{
			items[i] = oldArray[i];
		}
			
		delete [] oldArray;
			
	} // end if
	
	top++;
    items[top] = newEntry;
    return true;
}  // end push


template<class ItemType>
bool ArrayStack<ItemType>::pop()
{
    bool result = false;
    if (!isEmpty())
    {
        result = true;
        top--;
    }  // end if

    return result;
}  // end pop


template<class ItemType>
ItemType ArrayStack<ItemType>::peek() const throw(PrecondViolatedExcep)
{
    // Enforce precondition
    if (isEmpty())
        throw PrecondViolatedExcep("peek() called with empty stack");

    // Stack is not empty; return top
    return items[top];
}  // end peek

// End of implementation file.
